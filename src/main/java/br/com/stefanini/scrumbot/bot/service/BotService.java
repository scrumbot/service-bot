package br.com.stefanini.scrumbot.bot.service;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.UpdatesListener;
import com.pengrad.telegrambot.model.Update;

import br.com.stefanini.scrumbot.bot.client.ProjetoClient;
import br.com.stefanini.scrumbot.bot.dto.ProjetoDTO;
import br.com.stefanini.scrumbot.bot.provider.ApplicationContextProvider;

public class BotService {

	private ProjetoDTO projeto;
	public static TelegramBot bot;
	private static final int THREADS = 4;
	private ExecutorService threadExecutor;
	
	public BotService(Integer idProjeto) {
		ProjetoClient projetoClient = ApplicationContextProvider.getApplicationContext().getBean(ProjetoClient.class);
		this.projeto = projetoClient.obterProjeto(idProjeto);
		
		this.threadExecutor = Executors.newFixedThreadPool(THREADS);
		this.iniciarBot();
	}
	
	private void iniciarBot() {
		if(projeto.getTokenBot() != null) {
			if(bot != null) {
				bot.removeGetUpdatesListener();
			}
			bot =  new TelegramBot(projeto.getTokenBot());
		}
	}
	
	public void iniciarListener() {
		bot.setUpdatesListener(new UpdatesListener() {
			
			@Override
			public int process(List<Update> updates) {
				updates.forEach(update -> {
					BotExecutor botExecutor = ApplicationContextProvider.getApplicationContext().getBean(BotExecutor.class);
					threadExecutor.execute(botExecutor.iniciar(update, projeto));
				});
				return CONFIRMED_UPDATES_ALL;
			}
		});
	}
}
