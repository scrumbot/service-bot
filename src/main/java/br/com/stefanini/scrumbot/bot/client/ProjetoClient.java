package br.com.stefanini.scrumbot.bot.client;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.stefanini.scrumbot.bot.dto.ProjetoDTO;

@FeignClient("access")
public interface ProjetoClient {

	@GetMapping("/projetos/{id}")
	ProjetoDTO obterProjeto(@PathVariable("id") Integer id);
	
	@GetMapping("/relatorios")
	String obterRelatorio(@RequestParam("idProjeto") Integer idProjeto, 
						  @RequestParam("usuario") String usuario,
						  @RequestParam("comando") String comando);
	
	@GetMapping("/relatorios/{idProjeto}/{usuario}")
	List<String> obterComandosRelatorios(@PathVariable("idProjeto") Integer idProjeto, 
						  @PathVariable("usuario") String usuario);
	
	
}
