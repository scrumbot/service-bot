package br.com.stefanini.scrumbot.bot.model;

import org.springframework.data.annotation.Id;

import br.com.stefanini.scrumbot.bot.enums.StatusMensagem;

public class Mensagem {

	@Id
	private String id;

	private Integer idChat;
	private Long idUsuario;
	private Integer idMensagem;
	private String nomeUsuario;
	private String mensagem;
	private StatusMensagem status;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getIdMensagem() {
		return idMensagem;
	}

	public void setIdMensagem(Integer idMensagem) {
		this.idMensagem = idMensagem;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public Integer getIdChat() {
		return idChat;
	}

	public void setIdChat(Integer idChat) {
		this.idChat = idChat;
	}

	public String getNomeUsuario() {
		return nomeUsuario;
	}

	public void setNomeUsuario(String nomeUsuario) {
		this.nomeUsuario = nomeUsuario;
	}

	public StatusMensagem getStatus() {
		return status;
	}

	public void setStatus(StatusMensagem status) {
		this.status = status;
	}

	public Long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public boolean naoRespondida() {
		return StatusMensagem.NAO_RESONDIDA.equals(this.status);
	}

}
