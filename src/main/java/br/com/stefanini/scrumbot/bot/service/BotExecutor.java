package br.com.stefanini.scrumbot.bot.service;

import java.net.UnknownHostException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.springframework.dao.DuplicateKeyException;

import com.pengrad.telegrambot.model.Update;
import com.pengrad.telegrambot.model.request.ChatAction;
import com.pengrad.telegrambot.request.SendChatAction;
import com.pengrad.telegrambot.request.SendMessage;

import br.com.stefanini.scrumbot.bot.client.ProjetoClient;
import br.com.stefanini.scrumbot.bot.client.TFSClient;
import br.com.stefanini.scrumbot.bot.dto.BacklogResquestDTO;
import br.com.stefanini.scrumbot.bot.dto.ProjetoDTO;
import br.com.stefanini.scrumbot.bot.dto.WorkItemFieldsDTO;
import br.com.stefanini.scrumbot.bot.enums.StatusMensagem;
import br.com.stefanini.scrumbot.bot.exception.ReportNotFoundException;
import br.com.stefanini.scrumbot.bot.exception.UnauthorizedException;
import br.com.stefanini.scrumbot.bot.model.Mensagem;
import br.com.stefanini.scrumbot.bot.provider.ApplicationContextProvider;
import br.com.stefanini.scrumbot.bot.repository.MensagemRepository;

public class BotExecutor implements Runnable {

	private static final String QUEBRA_LINHA = "\n";
	private Update update;
	private ProjetoDTO projeto;

	private MensagemRepository mensagemRepository;

	private ProjetoClient projetoClient;
	private TFSClient tfsClient;

	public BotExecutor(MensagemRepository mensagemRepository) {
		this.mensagemRepository = mensagemRepository;
	}

	public BotExecutor iniciar(Update update, ProjetoDTO projeto) {
		this.update = update;
		this.projeto = projeto;
		return this;
	}

	@Override
	public void run() {
		Mensagem mensagem = null;

		try {
			mensagem = criarMensagem(update);
			getMensagemRepository().save(mensagem);

			BotService.bot.execute(new SendChatAction(mensagem.getIdUsuario(), ChatAction.typing.name()));
			String consulta = getProjetoClient().obterRelatorio(this.projeto.getId(), mensagem.getNomeUsuario(),
					mensagem.getMensagem());

			BotService.bot.execute(new SendChatAction(mensagem.getIdUsuario(), ChatAction.typing.name()));
			List<WorkItemFieldsDTO> workItems = getTFSClient().obterBacklogs(criarBacklogResquest(consulta));
			String resultado = construirMensagemWorkItem(workItems);

			BotService.bot.execute(new SendMessage(mensagem.getIdUsuario(), resultado));

			mensagem.setStatus(StatusMensagem.RESPONDIDA);
			getMensagemRepository().save(mensagem);

		} catch (ReportNotFoundException e) {
			BotService.bot.execute(
					new SendMessage(mensagem.getIdUsuario(), construirRepostaPadraoRelatorioNaoEncontrado(mensagem)));

		} catch (UnauthorizedException e) {
			BotService.bot.execute(new SendMessage(mensagem.getIdUsuario(),
					"Você não tem permissão para acessar o relatório informado."));

		} catch (DuplicateKeyException e) {
			tratarErroMensagemDuplicada(mensagem);

		} catch (Exception e) {
			tratarIndisponibilidadeServidor(e, mensagem);

		}
	}

	private String construirMensagemWorkItem(List<WorkItemFieldsDTO> items) {
		if (!items.isEmpty()) {
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append("Horário consulta: "
					+ LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
			stringBuilder.append(QUEBRA_LINHA);
			stringBuilder.append(QUEBRA_LINHA);

			items.forEach(item -> {
				stringBuilder.append(item.toString());
				stringBuilder.append(QUEBRA_LINHA);
			});

			return stringBuilder.toString();
		}
		return "A consulta não retornou resultados.";
	}

	private String construirRepostaPadraoRelatorioNaoEncontrado(Mensagem mensagem) {
		StringBuilder respostaPadrao = new StringBuilder("Não foi encontrado um relatório com o comando informado.");
		List<String> comandosDisponiveisPorUsuario = getProjetoClient().obterComandosRelatorios(projeto.getId(),
				mensagem.getNomeUsuario());
		if (!comandosDisponiveisPorUsuario.isEmpty()) {
			respostaPadrao.append(QUEBRA_LINHA);
			respostaPadrao.append("Tente um dos comandos abaixo:");
			respostaPadrao.append(QUEBRA_LINHA);
			respostaPadrao.append(comandosDisponiveisPorUsuario);
		}
		return respostaPadrao.toString();
	}

	private BacklogResquestDTO criarBacklogResquest(String consulta) {
		return new BacklogResquestDTO(projeto.getTokenAcesso(), consulta, projeto.getColecao(), projeto.getNome());
	}

	private MensagemRepository getMensagemRepository() {
		if (this.mensagemRepository == null) {
			this.mensagemRepository = ApplicationContextProvider.getApplicationContext()
					.getBean(MensagemRepository.class);
		}
		return this.mensagemRepository;
	}

	private ProjetoClient getProjetoClient() {
		if (this.projetoClient == null) {
			this.projetoClient = ApplicationContextProvider.getApplicationContext().getBean(ProjetoClient.class);
		}
		return this.projetoClient;
	}

	private TFSClient getTFSClient() {
		if (this.tfsClient == null) {
			this.tfsClient = ApplicationContextProvider.getApplicationContext().getBean(TFSClient.class);
		}
		return this.tfsClient;
	}

	private Mensagem criarMensagem(Update update) {
		Mensagem mensagem = new Mensagem();
		mensagem.setMensagem(update.message().text());
		mensagem.setIdUsuario(update.message().chat().id());
		mensagem.setIdMensagem(update.message().messageId());
		mensagem.setIdChat(update.updateId());
		mensagem.setNomeUsuario(update.message().from().username());
		mensagem.setStatus(StatusMensagem.RESPONDENDO);
		return mensagem;
	}

	private void tratarIndisponibilidadeServidor(Exception e, Mensagem mensagem) {
		if (e instanceof UnknownHostException) {
			mensagem.setStatus(StatusMensagem.NAO_RESONDIDA);
			getMensagemRepository().save(mensagem);
			BotService.bot.execute(new SendMessage(mensagem.getIdUsuario(),
					"Não foi possível estabelecer uma conexão com o servidor. Tentaremos novamente em alguns minutos."));
		} else {
			BotService.bot.execute(new SendMessage(mensagem.getIdUsuario(),
					"Ocorreu um erro ao processar o seu pedido. Tente novamente em alguns minutos."));
		}
	}

	private void tratarErroMensagemDuplicada(Mensagem mensagem) {
		System.out.println("Mensagem duplicada: " + mensagem.getMensagem());
	}

}
