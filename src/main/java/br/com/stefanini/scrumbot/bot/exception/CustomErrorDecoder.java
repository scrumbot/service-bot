package br.com.stefanini.scrumbot.bot.exception;

import java.net.UnknownHostException;

import feign.codec.ErrorDecoder;

public class CustomErrorDecoder implements ErrorDecoder {

	@Override
	public Exception decode(String methodKey, feign.Response response) {
		switch (response.status()) {
		case 400:
			return new BusinessException();
		case 401:
			return new UnauthorizedException();
		case 404:
			return new ReportNotFoundException();
		default:
			return new UnknownHostException();
		}
	}

}
