package br.com.stefanini.scrumbot.bot.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.commons.util.InetUtils;
import org.springframework.cloud.netflix.eureka.EurekaInstanceConfigBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import com.netflix.appinfo.AmazonInfo;

import br.com.stefanini.scrumbot.bot.exception.CustomErrorDecoder;
import br.com.stefanini.scrumbot.bot.repository.MensagemRepository;
import br.com.stefanini.scrumbot.bot.service.BotExecutor;
import feign.codec.ErrorDecoder;

@Configuration
public class Config {

	@Bean
	BotExecutor botExecutor(MensagemRepository mensagemRepository) {
		return new BotExecutor(mensagemRepository);
	}

	@Bean
	public ErrorDecoder errorDecoder() {
		return new CustomErrorDecoder();
	}
	
	@Value("${server.port}") 
    private int port;
	
	@Bean
	@Profile("!dev")
	public EurekaInstanceConfigBean eurekaInstanceConfig(InetUtils inetUtils) {
		EurekaInstanceConfigBean eurekaInstanceConfigBean = new EurekaInstanceConfigBean(inetUtils);
		AmazonInfo info = AmazonInfo.Builder.newBuilder().autoBuild("eureka");
		eurekaInstanceConfigBean.setDataCenterInfo(info);
		eurekaInstanceConfigBean.setHostname(info.get(AmazonInfo.MetaDataKey.localHostname));
		eurekaInstanceConfigBean.setIpAddress(info.get(AmazonInfo.MetaDataKey.localIpv4));
		eurekaInstanceConfigBean.setNonSecurePort(port);
		return eurekaInstanceConfigBean;
	}

}
