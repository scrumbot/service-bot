package br.com.stefanini.scrumbot.bot.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.stefanini.scrumbot.bot.service.BotService;

@RestController
@RequestMapping(value = "/bot")
public class BotController {
	
	
	@PostMapping("/iniciar")
	public ResponseEntity<?> iniciar(@RequestBody Integer idProjeto) {
		
		BotService botService = new BotService(idProjeto);
		botService.iniciarListener();
		return ResponseEntity.ok().build();
	}
	
	@PostMapping("/parar")
	public ResponseEntity<?> parar() {
		
		BotService.bot.removeGetUpdatesListener();
		return ResponseEntity.ok().build();
	}
	
}
