package br.com.stefanini.scrumbot.bot;

import java.util.logging.Logger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.core.env.Environment;

import br.com.stefanini.scrumbot.bot.provider.ApplicationContextProvider;
import br.com.stefanini.scrumbot.bot.service.BotService;

@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
@EnableCircuitBreaker
public class ServiceBotApplication {
	
	private static final int TRINTA_SEGUNDOS = 30000;
	private static final int NUMERO_TENTTIVAS = 10;
	private static final Logger LOG = Logger.getLogger(ServiceBotApplication.class.getName());

	public static void main(String[] args) throws Exception {
		SpringApplication.run(ServiceBotApplication.class, args);
		initializerBot();
		
	}
	
	public static void initializerBot() throws Exception {
		int tentativas = 1;
		LOG.info("Iniciando bot");

		String idProjeto = getEnvironment().getProperty("idProjeto");
		LOG.info("IdProjeto: " + idProjeto);

		if (idProjeto != null) {

			while (tentativas <= NUMERO_TENTTIVAS) {
				LOG.info("Iniciando serviço de descoberta. Tentativa: " + tentativas);

				try {
					new BotService(Integer.parseInt(idProjeto)).iniciarListener();
					LOG.info("Serviço iniciado com sucesso");
					return;
				} catch (Exception e) {
					LOG.warning("Falha ao iniciar o serviço. Tentativa: " + tentativas);
					Thread.sleep(TRINTA_SEGUNDOS);
				}
				tentativas++;
			}

			LOG.severe("Não foi possível iniciar o serviço.");
		}
		
	}
	
	public static Environment getEnvironment() {
		return ApplicationContextProvider.getApplicationContext().getBean(Environment.class);
	}
}
