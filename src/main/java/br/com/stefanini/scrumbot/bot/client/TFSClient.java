package br.com.stefanini.scrumbot.bot.client;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

import br.com.stefanini.scrumbot.bot.dto.BacklogResquestDTO;
import br.com.stefanini.scrumbot.bot.dto.WorkItemFieldsDTO;

@FeignClient("tfsconnection")
public interface TFSClient {

	@PostMapping("/tfs/backlogs")
	List<WorkItemFieldsDTO> obterBacklogs(BacklogResquestDTO resquest);
}
