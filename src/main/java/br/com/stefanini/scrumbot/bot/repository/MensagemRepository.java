package br.com.stefanini.scrumbot.bot.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import br.com.stefanini.scrumbot.bot.model.Mensagem;

@Repository
public interface MensagemRepository extends MongoRepository<Mensagem, String> {

	public Mensagem findByIdChatAndIdMensagemAndNomeUsuario(Long idChat, Integer idMensagem, String nomeUsuario);
}
