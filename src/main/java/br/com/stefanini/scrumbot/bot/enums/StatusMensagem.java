package br.com.stefanini.scrumbot.bot.enums;

public enum StatusMensagem {
	NAO_RESONDIDA,
	RESPONDENDO,
	RESPONDIDA;
}
